minishell: minishell.c
	gcc minishell.c -o minishell -L. -lparser_64

run: minishell
	./minishell 

debug: minishell.c
	gcc minishell.c -o minishell -L. -lparser_64 -g
clean:
	rm minishell parser.h.gch *~
