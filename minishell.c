#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include "parser.h"
#include <unistd.h>
#include <sys/stat.h>

// Constantes.
#define MAX_PATH 2048
#define PRONT '$'
#define MAX_INPUT_SZ 256

// Cabecera de funciones.
tline* parser(char* entrada);
int ejecutar(tline* entrada);
void manejador_igSennal(int sig);
int mycd(int argc, char *argv[]);


int main(int argc, char* argv[]){
    char entrada[MAX_INPUT_SZ];
    tline* entrada_parseada;

    // Ignoramos señales.
    signal(SIGINT,SIG_IGN);
    signal(SIGQUIT, SIG_IGN);

    while(0 == 0) {
	// Lee entrada por teclado.
	printf("%c ", PRONT);
	fgets(entrada,MAX_INPUT_SZ,stdin);

	if(strlen(entrada) > 1) {
	    // Parseamos la entrada.
	    entrada_parseada = parser(entrada);
	    if(entrada_parseada == NULL){
		printf("Error en entrada parseada.\n");
		return 1;
	    }

	    // Ejecutamos los comandos
	    ejecutar(entrada_parseada);
	}
    }

    return 0;
}

int ejecutar(tline* entrada) {
    int cont = 0;
    pid_t pid, pid_2;
    mode_t permisos = S_IRWXU | S_IRWXG | S_IRWXO;
    int mypipe[2];
    char error[1048] = "";
    FILE* fichero_error = NULL;
    int foutput = 0;
    int finput = 0;
    int estado = 0;

    // Comprobamos si hay que ejecutar el comando cd.
    if(strcmp(entrada->commands[0].argv[0],"cd") == 0){
	mycd(entrada->commands[0].argc, entrada->commands[0].argv);
    } else {
	// Redirecciones de entrada.
	if(entrada->redirect_input != NULL){
	    finput = open(entrada->redirect_input,O_RDONLY);
	    if(finput < 0){
		printf("%s: Error. Error al abrir el fichero.\n", entrada->redirect_input);
		return 1;
	    }
	}
	// Redirecciones de salida.
	if(entrada->redirect_output != NULL){
	    foutput = creat(entrada->redirect_output,permisos);
	    if(foutput < 0){
		printf("%s: Error. Error al abrir el fichero.\n", entrada->redirect_output);
		return 1;
	    }
	}

	// Redirecciones de error.
	if(entrada->redirect_error != NULL){
	    fichero_error = fopen(entrada->redirect_error,"a");
	    if(fichero_error == NULL){
		printf("%s: Error. Error al abrir el fichero.\n", entrada->redirect_error);
		return 1;
	    }
	}

	pid = fork();
	if(pid < 0){
	    // El proceso hijo ha fallado.
	    printf("El proceso hijo ha fallado.\n");
	    exit(-1);
	} else if(pid == 0){
	    // Poceso hijo.
	    // Control de background  y foreground.
	    if(entrada->background == 1){
		// el comando se ejecuta en background.
		printf("%d\n",(int) getpid());
	    } else {
		// comando en foreground.
		// restablecemos la señales de Ctrl + C y Ctrl + \
		signal(SIGINT,SIG_DFL);
		signal(SIGQUIT,SIG_DFL);
	    }

	    pipe(mypipe);

	    //  Recorremos todos los comandos.
	    for(cont = 0; cont < entrada->ncommands; cont++){
		if(entrada->commands[cont].filename == NULL) {
		    // El comando no existe.
		    if(entrada->redirect_error == NULL) {
			// Sin redireccion de error.
			printf("%s: No se encuentra el mandato.\n", entrada->commands[cont].argv[0]);
		    }  else {
			// Con redireccion de error.
			strcat(error, entrada->commands[cont].argv[0]);
			strcat(error, ": No se encuentra el mandato.");
			fputs(error, fichero_error);
			fclose(fichero_error);
		    }
		    break;
		} else {
		    // El comando existe.
		    pid_2 = fork();
		    if(pid_2 == 0){
			if(cont + 1 < entrada->ncommands){
			    // quedan comandos por ejecutar.
			    close(mypipe[0]);
			    dup2(mypipe[1],1);
			} else if((cont + 1 == entrada->ncommands) && (entrada->redirect_output != NULL)){
			    // en caso del que el ultimo comando tenga redireccion de salida.
			    dup2(foutput, 1);
			    close(foutput);
			}

			if((cont == 0) && (entrada->redirect_input != NULL)){
			    // primer comando con redereccion de entrada.
			    dup2(finput,0);
			    close(finput);
			} else if(cont > 0){
			    // se han ejecutado mas de un comando.
			    close(mypipe[1]);
			    dup2(mypipe[0], 0);
			}
			// Ejecutamos el comando.
			execvp(entrada->commands[cont].filename, entrada->commands[cont].argv);
		    }
		}
	    }
	     wait(NULL);
	    // waitpid
	    exit(0);
	} else {
	    // proceso  padre
	    if(entrada->background != 1){
		// Si el comando o los comandos se han ejecutado en foreground espero por mis 
		waitpid(pid,&estado,0);
	    }
	}
    }
    return 0;
}

void manejador_igSennal(int sig) {
    signal(SIGINT,manejador_igSennal);
    fflush(stdout);
}

int mycd(int argc, char *argv[]){
    char* path;
    char* path_r;
    char* pwd;
    char buffer[MAX_PATH];

    if(argc > 2){
	printf("Numero de parametros incorrectos.\n");
    } else if (argc == 1) {
	path = malloc(sizeof(char) * strlen(getenv("HOME")));
	path[0] = '\0';
	if(path == NULL) {
	    printf("Error al reservar memoria para el path.\n");
	    return 1;
	}
	strcpy(path,getenv("HOME"));
	chdir(path);
	printf("%s\n",path);
    } else {
	path = malloc(sizeof(char) * strlen(argv[1]));
	path[0] = '\0';
	if(path == NULL){
	    printf("Error al reservar memoria para path.\n");
	    return 1;
	}
	strcpy(path,argv[1]);
	if(path[0] == '.'){
	    path_r = malloc(sizeof(char) * MAX_PATH);
	    path_r[0] = '\0';
	    if(path_r == NULL) {
		printf("Error al reservar memoria para path_r.\n");
		return 1;
	    }
	    path_r = getcwd(buffer, sizeof(buffer));
	    strcat(path_r,"/");
	    strcat(path_r,path);
	    chdir(path_r);
	    pwd = malloc(sizeof(path_r));
	    pwd[0] = '\0';
	    if(pwd == NULL) {
		printf("Error al reservar memoria para pwd.\n");
		return 1;
	    }
	    pwd = getcwd(buffer, sizeof(buffer));
	    printf("%s\n", pwd);
	} else {
	    chdir(path);
	    pwd = malloc(sizeof(path));
	    pwd[0] = '\0';
	    if(pwd == NULL) {
		printf("Error al reservar memoria para pwd.\n");
		return 1;
	    }
	    pwd = getcwd(buffer, sizeof(buffer));
	    printf("%s\n", pwd);
	}
    }
    return 0;

}

tline* parser(char* entrada) {
    tline* analizado;

    analizado = malloc(sizeof(tline));
    if(analizado == NULL){
	printf("Error en la reserva de analizado.\n");
	return NULL;
    }

    analizado = tokenize(entrada);
    return analizado;
}
